import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'


Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: null,
        auth: false
    },
    mutations: {
        SET_USER(state, user) {
            state.user = user;
        }
    },
    actions: {
        async login({ dispatch }, credentials) {
            await axios.get("/sanctum/csrf-cookie");
            await axios.post("/login", credentials);
            dispatch('getUser');
        },
        getUser({ commit }) {
            axios.get("/api/user").then(res => {
                commit("SET_USER", res.data);
            }).catch(() => {
                commit("SET_USER", null);
            });;
        }
    },
    modules: {}
})