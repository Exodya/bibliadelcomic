/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

import VueRouter from 'vue-router';

Vue.use(VueRouter)

import Index from "./pages/Index.vue";
import About from "./pages/About.vue";
import Comic from "./pages/Comics.vue";
import Contact from "./pages/Contact.vue";
import Login from "./pages/Login.vue";
import Error from "./pages/404.vue";
import Character from "./slugs/Characters.vue";



const router = new VueRouter({
    mode: 'history',
    routes: [{
            path: '/',
            component: Index
        },
        {
            path: '/about',
            component: About
        },
        {
            path: '/comics',
            component: Comic
        },
        {
            path: '/contact-me',
            component: Contact
        },
        {
            path: '/about/:slug',
            component: Character
        },
        {
            path: '/login',
            component: Login
        },
        {
            path: '*',
            component: Error
        },
    ]
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('app', require('./layouts/default.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});